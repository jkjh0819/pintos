#include "vm/frame.h"
#include "vm/page.h"
#include "vm/swap.h"
#include "userprog/pagedir.h"
#include "filesys/file.h"
#include "threads/malloc.h"
#include "threads/palloc.h"
#include "threads/synch.h"
#include "threads/thread.h"
#include "userprog/syscall.h"

/* initialize LRU list */
void 
lru_list_init(void)
{
	list_init(&lru_list);
	lock_init(&lru_list_lock);
	lru_clock = NULL;
}

/* insert page to LRU list end */
void 
add_page_to_lru_list(struct page* page)
{
	lock_acquire(&lru_list_lock);
	list_push_back(&lru_list, &page->lru);
	lock_release(&lru_list_lock);
}

/* remove page from LRU list */
void 
del_page_from_lru_list(struct page* page)
{		
	list_remove(&page->lru);		
}

/* allocate page */
struct page* 
alloc_page(enum palloc_flags flags)
{
	struct page * page;

	/* allocation memory */
	void* kaddr = palloc_get_page(flags);	
	while(!kaddr)
	{	      
		
		lock_acquire(&lru_list_lock);
		kaddr=try_to_free_pages(flags);	
		lock_release(&lru_list_lock);				
	}
	
	/* Create and Initialize page */
	page = (struct page *)malloc(sizeof(struct page));
	if(page == NULL)
		return NULL;

	page->kaddr = kaddr;
	page ->thread = thread_current();
	page->vme = NULL;

	/* insert to LRU list */
	add_page_to_lru_list(page);

	return page;	
}

/* free page allocated in kaddr(physical address) */
void 
free_page(void *kaddr)
{
	struct list_elem *e;		
	for(e = list_begin(&lru_list); 
		e != list_end(&lru_list); e = list_next(e))
	{
		struct page *page = list_entry(e, struct page, lru);

		/* search page according to kaddr, if match cal __free_page */
		if(page->kaddr == kaddr)
		{
			__free_page(page);			
			break;
		}
	}		
}

/* free page in LRU list */
void 
__free_page(struct page* page)
{
	del_page_from_lru_list(page);
	pagedir_clear_page(page->thread->pagedir, page->vme->vaddr);
	palloc_free_page(page->kaddr);
	free(page);
}

/* return next list of LRU list */
static struct list_elem* 
get_next_lru_clock(void)
{
	struct list_elem * next;

  	next = list_next(lru_clock);

	/* if current LRU list is end, return NULL */
	if(next == list_end(&lru_list))
		next = NULL;
	
	return next;
}

/* use Clock algorithm, select victim and get out*/
void* 
try_to_free_pages (enum palloc_flags flags)
{  
  	if(list_empty(&lru_list))
  	{
  		lru_clock = NULL;
		return NULL;
  	}  

 	if(!lru_clock) 
  		lru_clock = list_begin(&lru_list);
  
	while (lru_clock)
  	{       
      		struct list_elem * e = get_next_lru_clock();	
      		struct page *page = list_entry(lru_clock, struct page, lru);
      
	  	struct thread *t = page->thread;

	  	if(pagedir_is_accessed(t->pagedir, page->vme->vaddr))
	  	{
	        	pagedir_set_accessed(t->pagedir, page->vme->vaddr, false);
	  	}
	  	else
	  	{
			/* if dirty bit is 1 */
			if(pagedir_is_dirty(t->pagedir, page->vme->vaddr) || page->vme->type == VM_ANON){
				/* write content to file */
				if(page->vme->type == VM_FILE)
				{
					lock_acquire(&filesys_lock);
					file_write_at(page->vme->vaddr, page->kaddr, page->vme->read_bytes, page->vme->offset);
					lock_release(&filesys_lock);
				}

				/* use swap_out and chagne type to VM_ANON */
				else if(page->vme->type == VM_BIN)
				{
					page->vme->type = VM_ANON;
					page->vme->swap_slot = swap_out(page->kaddr);
				}
		
				/* use swap_out */
				else if(page->vme->type == VM_ANON)
					page->vme->swap_slot = swap_out(page->kaddr);
			}

			/* free pages */
			page->vme->is_loaded = false;
      			del_page_from_lru_list(page);
      			palloc_free_page(page->kaddr);
       			pagedir_clear_page(t->pagedir, page->vme->vaddr);
       			free(page);
			lru_clock = e;
	        	return palloc_get_page(flags);
	 	}
		lru_clock = e;
   	}
	return NULL;
}

void
free_page_all(tid_t tid)
{
	struct list_elem * e;
	struct list_elem * next;
  	struct page * page;
  	for(e = list_begin(&lru_list); 
		e != list_end(&lru_list);)
	{
    		next = list_next(e);
		page = list_entry(e, struct page, lru);
		if(page->thread->tid == tid)
		{
		      del_page_from_lru_list(page);
		}
	   	e = next;
 	}
}
