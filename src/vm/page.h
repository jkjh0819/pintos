#ifndef VM_PAGE_H
#define VM_PAGE_H

#include <hash.h>
#include "threads/thread.h"

//ELF format files
#define VM_BIN 0

//a page of normal file
#define VM_FILE 1

//a page of swapp area
#define VM_ANON 2

struct vm_entry{
	uint8_t type; /* VM_BIN, VM_FILE, VM_ANON */
	void * vaddr; 
	bool writable; /* true -> enable write */

	bool is_loaded; /* physical memory loaded */
	struct file* file;

	struct list_elem mmap_elem; /* mmap list element */

	size_t offset;
	size_t read_bytes;
	size_t zero_bytes;

	size_t swap_slot;

	struct hash_elem elem; /* hash table element */
};

void vm_init(struct hash * vm);
bool insert_vme(struct hash * vm, struct vm_entry * vme);
bool delete_vme(struct hash * vm, struct vm_entry * vme);
struct vm_entry * find_vme(void * vaddr);
void vm_destroy(struct hash * vm);
bool load_file(void * kaddr, struct vm_entry * vme);

struct mmap_file{
	int mapid; /* after mmap() success, return value */
	struct file * file;
	struct list_elem elem;
	struct list vme_list;
};

struct page {
	void *kaddr; /* physical address of page */
	struct vm_entry * vme; /* pointer of vm_entry in virtual address */
	struct thread * thread; /* pointer of thread use physical page */
	struct list_elem lru;
};
#endif
