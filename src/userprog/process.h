#ifndef USERPROG_PROCESS_H
#define USERPROG_PROCESS_H

#include "threads/thread.h"
#include "filesys/filesys.h"
#include "vm/page.h"

tid_t process_execute (const char *file_name);
int process_wait (tid_t);
void process_exit (void);
void process_activate (void);
void process_close_file(int fd);
struct file* process_get_file(int fd);
int process_add_file(struct file * f);
void remove_child_process(struct thread * cp);
struct thread* get_child_process(int pid);
void argument_stack(char ** parse, int count, void ** esp);
bool handle_mm_fault(struct vm_entry * vme);

#endif /* userprog/process.h */
