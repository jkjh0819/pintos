#define F (1<<14)  	//fixed point 1
#define INT_MAX ((1 << 31) - 1)
#define INT_MIN (-1(1 << 31))
//x and y denote fixed_point numbers in 17.14 format
//n is an integer

int int_to_fp(int n);		//change integer to fixed point
int fp_to_int_round(int x);	//change fp to int by round
int fp_to_int(int x);		//change fp to int, erase under 0
int add_fp(int x, int y);	//add fp with fp
int add_mixed(int x, int n);	//add fp iwth int
int sub_fp(int x, int y);	//subtract fp with fp  x-y
int sub_mixed(int x, int n);	//subtract fp with int x-n
int mult_fp(int x, int y);	//multiple fp with fp
int mult_mixed(int x, int n);	//multiple fp with int
int div_fp(int x, int y);	//divide fp with fp  x/y
int div_mixed(int x, int n);	//divide fp with int x/n

int
int_to_fp(int n)
{
	return n * F;
}

int 
fp_to_int_round(int x)
{
	if(x >= 0)
		return (x + F / 2) / F;
	else
		return (x - F / 2) / F;
}

int 
fp_to_int(int x)
{
	return x / F;
}

int 
add_fp(int x, int y)
{
	return x + y;
}

int 
add_mixed(int x, int n)
{
	return x + n * F;
}

int 
sub_fp(int x, int y)
{
	return x - y;
}

int 
sub_mixed(int x, int n)
{
	return x - n * F;
}

int 
mult_fp(int x, int y)
{
	return ((int64_t) x) * y / F;
}

int 
mult_mixed(int x, int n)
{
	return x * n;
}

int 
div_fp(int x, int y)
{
	return ((int64_t) x) * F / y;
}

int 
div_mixed(int x, int n)
{
	return x / n;
}
