#include "filesys/filesys.h"
#include "filesys/inode.h"
#include "filesys/buffer_cache.h"
#include <stdio.h>
#include <string.h>
#include "threads/malloc.h"

void* p_buffer_cache;		//point to buffer cache memory
struct buffer_head buffer_head[BUFFER_CACHE_ENTRY_NB];		//array of buffer head
static int clock_hand;		//for clock

bool
bc_read (block_sector_t sector_i, void *buffer, off_t buffer_ofs, int chunk_size, int sector_ofs)
{
  bool success = false;
  struct buffer_head *lookup;
  
  //find sector_index
  lookup = bc_lookup(sector_i);

  if(lookup == NULL)
  {
    //get buffer head for cacheing data block
    lookup = bc_select_victim();
    if(lookup == NULL)
      return success;
    
    lock_acquire(&lookup->lock);
    block_read(fs_device, sector_i, lookup->data);

    lookup->dirty = false;
    lookup->valid = true;
    lookup->sector = sector_i;
    lock_release(&lookup->lock);
  }
  lock_acquire(&lookup->lock);
 
  //copy disk block data
  memcpy(buffer + buffer_ofs, lookup->data + sector_ofs, chunk_size);

  lookup->clock_bit = true;
  lock_release(&lookup->lock);

  success = true;

  return success;
}

bool
bc_write (block_sector_t sector_i, void *buffer, off_t buffer_ofs, int chunk_size, int sector_ofs)
{

  bool success = false;

  struct buffer_head *lookup;
  
  //find sector index
  lookup = bc_lookup(sector_i);

  if(lookup == NULL)
  {
	lookup = bc_select_victim();
	if(lookup == NULL)
	  return success;
	
	block_read(fs_device, sector_i, lookup->data);
  }
 
  lock_acquire(&lookup->lock);
  memcpy(lookup->data + sector_ofs, buffer + buffer_ofs, chunk_size);

  //update
  lookup->dirty = true;
  lookup->valid = true;
  lookup->sector = sector_i;
  lookup->clock_bit = true;
  lock_release(&lookup->lock);

  success = true;

  return success;
}

void
bc_init(void)
{

  int i;
  void* p_data;  

  //allocation buffer cache in memoery
  p_buffer_cache = malloc(BLOCK_SECTOR_SIZE*BUFFER_CACHE_ENTRY_NB);
  
  if(p_buffer_cache == NULL)
  {
    return;
  }
  else
  {
    p_data = p_buffer_cache;
  }

  //buffer_head initialize
  for(i=0; i<BUFFER_CACHE_ENTRY_NB; i++)
  {
    buffer_head[i].dirty = false;
    buffer_head[i].valid = false;
    buffer_head[i].sector = -1;
    buffer_head[i].clock_bit = 0;
    lock_init(&buffer_head[i].lock);
    buffer_head[i].data = p_data;

    p_data = p_data + BLOCK_SECTOR_SIZE;
  }

}

//flush all buffer cache entry and free
void
bc_term(void)
{
  bc_flush_all_entries();
  free(p_buffer_cache);
}

struct buffer_head*
bc_select_victim(void)
{
  int i;
  
  //select victim by clock algorithm
  while(1)
  {
    i = clock_hand;

    if(i == BUFFER_CACHE_ENTRY_NB)
      i = 0;

    if(++clock_hand == BUFFER_CACHE_ENTRY_NB)
      clock_hand = 0;

    if(buffer_head[i].clock_bit)
    {
      lock_acquire(&buffer_head[i].lock);
      buffer_head[i].clock_bit = 0;
      lock_release(&buffer_head[i].lock);
    }
    else
    {
      lock_acquire(&buffer_head[i].lock);
      buffer_head[i].clock_bit = 1;
      lock_release(&buffer_head[i].lock);
      break;
    }
  }

  //if dirty, flush to disk
  if(buffer_head[i].dirty == true){
    bc_flush_entry(&buffer_head[i]);
  }

  lock_acquire(&buffer_head[i].lock);
  
  //update buffer_head
  buffer_head[i].dirty = false;
  buffer_head[i].valid = false;
  buffer_head[i].sector = -1;
  
  lock_release(&buffer_head[i].lock);

  return &buffer_head[i];
}

struct buffer_head*
bc_lookup(block_sector_t sector)
{
  int i;

  for(i = 0; i < BUFFER_CACHE_ENTRY_NB; i++)
  {
    //if find success, return
    if(buffer_head[i].sector == sector)
      return &buffer_head[i];
  }
  return NULL;
}


void
bc_flush_entry(struct buffer_head *p_flush_entry)
{
  lock_acquire(&p_flush_entry->lock);
  block_write(fs_device, p_flush_entry->sector, p_flush_entry->data);
  p_flush_entry->dirty = false;
  lock_release(&p_flush_entry->lock);
}

void
bc_flush_all_entries(void)
{
  int i;

  for(i = 0; i < BUFFER_CACHE_ENTRY_NB; i++)
  {
	if(buffer_head[i].dirty == true)
	{
          lock_acquire(&buffer_head[i].lock);
	  block_write(fs_device, buffer_head[i].sector, buffer_head[i].data);
	  buffer_head[i].dirty = false;
          lock_release(&buffer_head[i].lock);
	}
  }
}



