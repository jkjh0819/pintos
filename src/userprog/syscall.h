#ifndef USERPROG_SYSCALL_H
#define USERPROG_SYSCALL_H

#include <threads/thread.h>
#include "vm/page.h"

#define CLOSE_ALL -1

typedef int pid_t;
typedef int mapid_t;

void syscall_init (void);
struct lock filesys_lock;

//system call functions
struct vm_entry * check_address(void * addr, void * esp UNUSED);
void get_argument(void * esp, int * arg, int count);
void halt(void);
void exit(int status);
bool create(const char * file, unsigned initial_size);
bool remove(const char * file);
pid_t exec(const char * cmd_line);
void close(int fd);
unsigned tell(int fd);
void seek(int fd, unsigned position);
int write(int fd, void * buffer, unsigned size);
int read(int fd, void * buffer, unsigned size);
int filesize(int fd);
int open(const char * file);
int wait(tid_t tid);

//check valid input and memory area
void check_valid_buffer(void * buffer, unsigned size, void * esp, bool to_write);
void check_valid_string(const void * str, void * esp);

//related to mmap assignment
int mmap(int fd, void * addr);
void munmap(mapid_t mapid);
void do_munmap(struct mmap_file * mmap_file);

#endif /* userprog/syscall.h */
