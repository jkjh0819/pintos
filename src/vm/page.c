#include "vm/page.h"
#include "threads/vaddr.h"
#include "threads/malloc.h"
#include <string.h>
#include "threads/palloc.h"
#include "userprog/pagedir.h"
#include "filesys/file.h"
#include "userprog/process.h"
#include "threads/interrupt.h"
#include "userprog/syscall.h"


/* return hash value */
static unsigned 
vm_hash_func(const struct hash_elem * e, void * aux UNUSED)
{
	/* search vm_entry struct by hash_entry */
	return hash_int((int)(hash_entry(e, struct vm_entry, elem)->vaddr));
}

/* compare vaddr of two hash_element */
static bool
vm_less_func(const struct hash_elem *a, const struct hash_elem * b UNUSED)
{
	/* search vm_entry and compare a < b? true : false */
	return (hash_entry(a, struct vm_entry, elem)->vaddr) < (hash_entry(b, struct vm_entry, elem)->vaddr);
}

static void
vm_destroy_func(struct hash_elem *e, void *aux UNUSED)
{
	/* get hash element */
	struct vm_entry *vme = hash_entry(e, struct vm_entry, elem);

	/* if loaded */
	if(vme->is_loaded)
	{
		/* free page and page mapping */
		palloc_free_page(pagedir_get_page(thread_current()->pagedir,vme->vaddr));
		pagedir_clear_page(thread_current()->pagedir,vme->vaddr);
	}
	/* free vm_entry object */
	free(vme);
}

/* return vm_entry according to vaddr */
struct 
vm_entry *find_vme(void *vaddr)
{
	struct vm_entry ref;
	struct hash_elem * ret;

	/* get page number according to vaddr */
	ref.vaddr = pg_round_down(vaddr);

	/* get struct hash_elem */
	ret = hash_find(&thread_current()->vm, &ref.elem);

	if(!ret) return NULL;
	return hash_entry(ret, struct vm_entry, elem);
}

/* insert vm_entry to hash table */
bool
insert_vme(struct hash *vm, struct vm_entry *vme)
{
	return hash_insert(vm,&vme->elem) == NULL;
}

/* delete vm_entry from hash table */
bool
delete_vme(struct hash *vm, struct vm_entry *vme)
{
	return hash_delete(vm,&vme->elem) != NULL;
}

/* Initialize vm in thread.h */
void
vm_init(struct hash * vm)
{
	hash_init(vm, vm_hash_func, vm_less_func, NULL );
}

/* remove hash table list and vm_entry */
void
vm_destroy(struct hash *vm)
{
	hash_destroy(vm, vm_destroy_func);
}

/* load file to physical page */
bool
load_file(void * kaddr, struct vm_entry * vme)
{
	if(vme->read_bytes > 0)
	{
		lock_acquire(&filesys_lock);
		
		/* write data to physical page */
		if(file_read_at(vme->file, kaddr, vme->read_bytes, vme->offset) != 
			(int)vme->read_bytes)
		{
			lock_release(&filesys_lock);
			return false;
		}
		lock_release(&filesys_lock);

		/* padding remained area by 0 */
		memset(kaddr + vme->read_bytes, 0, vme->zero_bytes);
	}
	else
		memset(kaddr, 0, PGSIZE);
	return true;
}
