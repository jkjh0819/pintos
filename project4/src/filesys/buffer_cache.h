#ifndef FILESYS_BUFFER_CACHE_H
#define FILESYS_BUFFER_CACHE_H

#include "threads/synch.h"

#define BUFFER_CACHE_ENTRY_NB 64	//number of buffer cache entry, 32kb

struct buffer_head
{
  bool dirty;			//whether is dirty or not
  bool valid;			//whether is used or not
  block_sector_t sector;	//address of disk sector
  bool clock_bit;		//for clock
  struct lock lock;
  void *data;			//data pointer to buffer cache entry
};

bool bc_read(block_sector_t sector_idx, void *buffer, off_t buffer_ofs, int chunk_size, int sector_ofs);
bool bc_write (block_sector_t sector_idx, void *buffer, off_t buffer_ofs, int chunk_size, int sector_ofs);
void bc_init(void);
void bc_term(void);
struct buffer_head* bc_lookup(block_sector_t sector);
struct buffer_head* bc_select_victim(void);
void bc_flush_entry(struct buffer_head*);
void bc_flush_all_entries(void);

#endif
