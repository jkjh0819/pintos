#include "userprog/syscall.h"
#include "userprog/process.h"
#include <stdio.h>
#include <syscall-nr.h>
#include "threads/interrupt.h"
#include "threads/thread.h"
#include "devices/shutdown.h"
#include "devices/input.h"
#include "filesys/filesys.h"
#include "filesys/file.h"
#include <stdlib.h>
#include "process.h"
#include <string.h>
#include "threads/vaddr.h"
#include "threads/malloc.h"
#include "userprog/pagedir.h"
#include "threads/palloc.h"
#include "vm/frame.h"

static void syscall_handler (struct intr_frame *);

//check address position whether the address in user region
struct vm_entry *
check_address(void * addr, void * esp UNUSED)
{
  //if address out from user region, process terminated
  if(addr < (void*)0x08048000 || !is_user_vaddr(addr))	
	exit(-1);

  /* if addr exists in vm_entry, return that */
  struct vm_entry * vme = find_vme(addr);
	
  return vme;
}

//close file opened, to remove file descriptor is acted in function process_close_file
void
close(int fd)
{
  process_close_file(fd);
}

//notice offset of open file
unsigned
tell(int fd)
{
  struct file * f = process_get_file(fd);
  if(f != NULL)
	return (unsigned)file_tell(f);
  return 0;
}

//move offset to current offset + position 
void 
seek(int fd, unsigned position)
{
  struct file * f = process_get_file(fd);
  if(f != NULL)
	file_seek(f, position);
}


//write data to file at fd index
int
write(int fd, void * buffer, unsigned size)
{
  unsigned write_size = -1;
  struct file * f;
  //preventing other thread change data
  lock_acquire(&filesys_lock);
  
  if(fd < 1);  

  //if fd == 1(STDOUT), print data saved in buffer to shell
  else if(fd == 1)
  {
  	lock_release(&filesys_lock);
	putbuf((char*)buffer,size);
	return size;
  }
  else
  {
    f= process_get_file(fd);
    
    if(f==NULL)
    {
   	  lock_release(&filesys_lock);
	  return -1;
    }

    write_size = file_write(f, buffer, size);
  }
  lock_release(&filesys_lock);
  return write_size;
}

//read data from file at fd
int
read(int fd, void *buffer, unsigned size)
{
  unsigned count;
  //prevent other thread change data
  lock_acquire(&filesys_lock);
  struct file * f = process_get_file(fd);

  //if fd == 0(STDIN) read from STDIN
  if(fd == 0)
  {
	for(count = 0; count < size;count++)
		((char*)buffer)[count]=input_getc();
	lock_release(&filesys_lock);
	return size;
  }	
  
  if(f == NULL)
  {
	lock_release(&filesys_lock);
	return -1;
  }

  //else, read from file
  size = file_read(f,buffer,size);
  lock_release(&filesys_lock);
  return size;
}

//notice filesize of file at fd index
int
filesize(int fd)
{
  struct file * f = process_get_file(fd);
  if(f == NULL)
	return -1;
  else
	return file_length(f);
}

//save argument to kernel from user stack
void 
get_argument(void *esp, int *arg, int count)
{
  int i;
  /*set pointer to data. it has fake address because of argument_stack,
	it should plus 4*/
  void *arg_ptr = esp + 4;
  check_address(arg_ptr,esp);
  for(i=0;i<count;i++)
  {
    arg[i] = *(int*) arg_ptr;
    arg_ptr = arg_ptr + 4;
    check_address(arg_ptr, esp);
  }
  esp = esp + 4*(count+1);
}

//open file, filename is file variable
int
open(const char * file)
{
  int result;
  //prevent other thread change data, it's for syn-write
  lock_acquire(&filesys_lock);
  struct file * f = filesys_open(file);
  if(f == NULL)
    result = -1;
  else	
    result = process_add_file(f);
  lock_release(&filesys_lock);
  return result;
}

//terminate pintos
void
halt(void)
{
  shutdown_power_off();
}

//terminate current process
void
exit(int status)
{
  //get current process
  struct thread *t = thread_current();
  t->exit_status = status;

  //show status
  printf("%s: exit(%d)\n",t->name,status);
  thread_exit();
}

//create file, file variable mean new file name and pwd
bool
create(const char *file, unsigned initial_size)
{
  return filesys_create(file,initial_size);
}

//remove file, file variable mean file name and pwd to remove
bool
remove(const char *file)
{
  return filesys_remove(file);
}

//create child process and execute
pid_t
exec(const char *cmd_line)
{
	tid_t tid;
	struct thread * child;
	//create child process
	tid = process_execute(cmd_line);

	//wait until child process onto memory: use semaphore
	child  = get_child_process(tid);
	sema_down(&child->load_sema);

	if(child->program_loaded)
		return child->tid;
	else
		return TID_ERROR;
}

int
wait(tid_t tid)
{
  return process_wait(tid);
}

void
syscall_init (void) 
{
  intr_register_int (0x30, 3, INTR_ON, syscall_handler, "syscall");
  lock_init(&filesys_lock);
}

//system call handler
static void
syscall_handler (struct intr_frame *f UNUSED) 
{
  int arg[3]; //maximum args is 3
  check_address(f->esp, f->esp);
  switch(*(int*) f->esp)
  {
     case SYS_HALT:
       halt();
       break;
     case SYS_EXIT:
       get_argument(f->esp,arg,1);
       exit(arg[0]);
       break; 
     case SYS_CREATE:
       get_argument(f->esp,arg,2);
       check_valid_string((const void*)arg[0], f->esp);
       //check_address((void*)arg[0]);
       f->eax = create((const char *)arg[0], (unsigned)arg[1]);
       break;
     case SYS_REMOVE:
       get_argument(f->esp,arg,1); 
       check_valid_string((const void *)arg[0], f->esp);
       //check_address((void*)arg[0]);
       f->eax = remove((const char *)arg[0]);
       break;     
     case SYS_EXEC:
       get_argument(f->esp,arg,1);
       check_valid_string((const void *)arg[0], f->esp);
       //check_address((void*)arg[0]);
       f->eax = exec((const char *)arg[0]);
       break;       
     case SYS_WAIT:
       get_argument(f->esp,arg,1);
       f->eax = wait(arg[0]);
       break;
     case SYS_OPEN:
       get_argument(f->esp,arg,1);
       check_valid_string((const void *)arg[0], f->esp);
       //check_address((void*)arg[0]);
       f->eax = open((const char*)arg[0]);
       break;
     case SYS_FILESIZE:
       get_argument(f->esp,arg,1);
       f->eax = filesize(arg[0]);
       break;
     case SYS_READ:
       get_argument(f->esp,arg,3);
       check_valid_buffer((void*)arg[1], (unsigned)arg[2], f->esp, true);
       //check_address((void*)arg[1]);
       f->eax = read(arg[0], (void*)arg[1], arg[2]);
       break;
     case SYS_WRITE:
       get_argument(f->esp,arg,3);
       /* in pdf, at write system call, use check_valid_string 
	  but it has error at page-merge-mm test, so have to use check_valid_buffer */
       //check_valid_string((const void *)arg[1], f->esp);
       check_valid_buffer((void*)arg[1], (unsigned)arg[2], f->esp, false);
       //check_address((void*)arg[1]);
       f->eax = write(arg[0],(void*)arg[1],arg[2]);
       break;
     case SYS_SEEK:
       get_argument(f->esp,arg,2);
       seek(arg[0],arg[1]);
       break;
     case SYS_TELL :
       get_argument(f->esp,arg,1);
       tell(arg[0]);
       break;
     case SYS_CLOSE:
       get_argument(f->esp,arg,1);
       close(arg[0]);
       break;
     case SYS_MMAP:
       get_argument(f->esp,arg,2);
       f->eax = mmap(arg[0], (void*)arg[1]);
       break;
     case SYS_MUNMAP:
       get_argument(f->esp,arg,1);
       munmap(arg[0]);
       break;

     default:
       thread_exit();
  }
  //thread_exit ();
}

//use in here, therefore define here
void
check_valid_buffer(void * buffer, unsigned size, void *esp, bool to_write)
{
	unsigned i;
	char * ptr_buffer = (char *) buffer;

	for(i = 0; i< size; i++)
	{
		/* check address whether in user stack and get vm_entry */
		struct vm_entry * vme = check_address((void *) ptr_buffer, esp);
		if(vme != NULL && to_write)
		{
			if(!vme->writable)
				exit(-1);
		}
		ptr_buffer++;
	}
}

void
check_valid_string(const void * str, void * esp)
{
	/* check str vm_entry existence */
	check_address((void *)str, esp);
	while(*(char *)str != 0) 
	{
		str = (char *)str + 1;
		check_address((void *)str, esp);
	}	
}

int 
mmap(int fd, void * addr)
{
	struct mmap_file * mmap_file;
	struct file * prev_f = process_get_file(fd);
	struct thread * cur = thread_current();
	int offset = 0;
	int mapid;
	unsigned int length;

	/* invalid addr -> return error */
	if(!prev_f || addr < (void*)0x08048000 || !is_user_vaddr(addr) || pg_ofs(addr) != 0
		|| ((uint32_t) addr %PGSIZE)!= 0)
		return -1;

	struct file * file = file_reopen(prev_f);
	if(!file || file_length(file) == 0)
		return -1;

	length = file_length(file);

	/* allocation mapid */
	mapid = cur->mapid++;

	/* Create and Initialize mmap_flie */
	mmap_file = (struct mmap_file *)malloc(sizeof(struct mmap_file));
	list_init(&mmap_file->vme_list);
	mmap_file->file = file;
	mmap_file->mapid = mapid;

	while(length > 0)
	{
		unsigned int page_read_bytes = length < PGSIZE ? length : PGSIZE;
		unsigned int page_zero_bytes = PGSIZE - page_read_bytes;
		struct vm_entry * vme;

	    	if( find_vme( addr ) != NULL )
      		{
			munmap(mapid);
			return -1;
		}

		/* Create and Initialize vm_entry */
		vme = (struct vm_entry*) malloc (sizeof(struct vm_entry));
		
		if( vme == NULL )
		{
			munmap(mapid); 
		       return -1;
		}

		vme->type = VM_FILE;
    		vme->vaddr = addr;
     		vme->writable = true;
    		vme->is_loaded = false;
    		vme->file = file;
    		vme->offset = offset;
    		vme->read_bytes = page_read_bytes;
    		vme->zero_bytes = page_zero_bytes;

    		list_push_back( &mmap_file->vme_list, &vme->mmap_elem );
    		insert_vme( &thread_current()->vm, vme );

    		length -= page_read_bytes;
    		offset += page_read_bytes;
    		addr += PGSIZE;
	}

	list_push_back(&thread_current()->mmap_list, &mmap_file->elem);

	return mmap_file->mapid;
}

void 
munmap(mapid_t mapid)
{
	struct list_elem * e;
	for(e = list_begin(&thread_current()->mmap_list);
		e != list_end(&thread_current()->mmap_list);)
	{
		struct mmap_file * mmap_file = list_entry(e, struct mmap_file, elem);
		
		/* mapid matched -> remove mmap_file */
		if(mmap_file->mapid == mapid || mapid == CLOSE_ALL)
		{
			do_munmap(mmap_file);
			e = list_remove(&mmap_file->elem);
			free(mmap_file);
		}
		else
			e = list_next(e);
	}	
}

void
do_munmap(struct mmap_file * mmap_file)
{
	struct list_elem * e;
	struct file * file = mmap_file->file;

	for(e = list_begin(&mmap_file->vme_list);
		e != list_end(&mmap_file->vme_list);)
	{
		struct vm_entry * vme = list_entry(e, struct vm_entry, mmap_elem);

		if(vme->is_loaded)
		{
			if(pagedir_is_dirty(thread_current()->pagedir, vme->vaddr))
			{
				lock_acquire(&filesys_lock);
				file_write_at(vme->file, vme->vaddr, vme->read_bytes, vme->offset);
				lock_release(&filesys_lock);
			}
			free_page(pagedir_get_page(thread_current()->pagedir, vme->vaddr));
			pagedir_clear_page(thread_current()->pagedir, vme->vaddr);
		}
		
		e = list_remove(&vme->mmap_elem);
		delete_vme(&thread_current()->vm, vme);
		
		free(vme);
	}

	if(file)
	{
		lock_acquire(&filesys_lock);
		file_close(file);
		lock_release(&filesys_lock);
	}
}
