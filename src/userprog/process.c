#include "userprog/process.h"
#include <debug.h>
#include <inttypes.h>
#include <round.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <hash.h>
#include "userprog/gdt.h"
#include "userprog/pagedir.h"
#include "userprog/tss.h"
#include "filesys/directory.h"
#include "filesys/file.h"
#include "filesys/filesys.h"
#include "threads/flags.h"
#include "threads/init.h"
#include "threads/interrupt.h"
#include "threads/palloc.h"
#include "threads/thread.h"
#include "threads/vaddr.h"
#include "threads/malloc.h"
#include "userprog/syscall.h"
#include "vm/frame.h"
#include "vm/swap.h"

extern struct lock filesys_lock;

static thread_func start_process NO_RETURN;
static bool load (const char *cmdline, void (**eip) (void), void **esp);

//call file_close, and initialize file descriptor table fd index as NULL 
void
process_close_file(int fd)
{
	struct file * f = process_get_file(fd);
	if(f != NULL)
	{	
		file_close(f);
		thread_current()->fd_table[fd]=NULL;
	}
}

//check file descriptor where fd index and if it exists, return pointer
struct file *
process_get_file(int fd)
{
	struct thread * t = thread_current();
	if(t->fd_index < fd || t->fd_table[fd] == NULL )
		return NULL;
	return t->fd_table[fd];
}

//create file descriptor for f
int
process_add_file(struct file *f)
{
 	struct thread * t = thread_current();
	t->fd_table[t->fd_index++] = f;
	return t->fd_index -1;
}

/*remove process descriptor in child list of parent process*/
void
remove_child_process(struct thread * cp)
{
	list_remove(&cp->child_list_elem);
	palloc_free_page(cp);
} 

/*search child list by pid and return its descriptor*/
struct thread *
get_child_process(int pid)
{
	struct list_elem * e;
	for(e = list_begin(&thread_current()->child_list);
		e != list_end(&thread_current()->child_list);
		e=list_next(e))
	{
	  struct thread * t = list_entry(e,struct thread,child_list_elem);
	  if(t->tid==pid)
	    return t;
	}
	return NULL;
}

//save argument to stack
void
argument_stack(char **parse, int count, void **esp)
{
  char *token, **argv;
  int argc=count, i=count-1;

  //allocate memory for saving address
  argv = (char**)malloc(sizeof(char*)*(count+1));

  //first, save data to stack. 
  for(token = parse[i];i >= 0; token=parse[i])
  {
    *esp -= strlen(token) + 1;
    argv[i]=*esp;
    memcpy(*esp,token, strlen(token)+1);
    i--;
  }

  //its for zero address
  argv[argc]=0;

  //address size is 4. so match the esp as 4-multiple
  i = (size_t) *esp % 4;
  if(i)
  {
    *esp -= i;
    memcpy(*esp,&argv[argc],i);
  }

  //save the address argument saved to stack
  for(i = argc; i >= 0; i--)
  {
    *esp -= sizeof(char*);
    memcpy(*esp, &argv[i], sizeof(char*));
  }

  //move argv[0]address to token, and save the address to stack 
  token = *esp;
  *esp -= sizeof(char**);
  memcpy(*esp, &token, sizeof(char**));

  //save argv to stack
  *esp -= sizeof(int);
  memcpy(*esp, &argc, sizeof(int));

  //save fake address to stack
  *esp -= sizeof(void *);
  memcpy(*esp, &argv[argc], sizeof(void *));

  free(argv);
} 

/* Starts a new thread running a user program loaded from
   FILENAME.  The new thread may be scheduled (and may even exit)
   before process_execute() returns.  Returns the new process's
   thread id, or TID_ERROR if the thread cannot be created. */
tid_t
process_execute (const char *file_name) 
{
  char *fn_copy;
  tid_t tid;

  char *save_ptr, *temp_file_name;
  /* Make a copy of FILE_NAME.
     Otherwise there's a race between the caller and load(). */
  fn_copy = palloc_get_page (0);
  if (fn_copy == NULL)
    return TID_ERROR;
  strlcpy (fn_copy, file_name, PGSIZE);
 
  //file_name is const char, so allocate new memory for parsing file_name
  temp_file_name = palloc_get_page(0);
  if(temp_file_name == NULL)
    return TID_ERROR;
  strlcpy(temp_file_name, file_name, PGSIZE);

  //parsing file_name
  temp_file_name = strtok_r(temp_file_name," ",&save_ptr);
  
  /* Create a new thread to execute FILE_NAME. */
  tid = thread_create (temp_file_name, PRI_DEFAULT, start_process, fn_copy);
  if (tid == TID_ERROR)
    palloc_free_page (fn_copy);
  
  //return memory temp_file_name, it affects multi-oom test
  palloc_free_page(temp_file_name);

  return tid;
}

/* A thread function that loads a user process and starts it
   running. */
static void
start_process (void *file_name_)
{
  char *file_name = file_name_;
  struct intr_frame if_;
  bool success;
  char **parse, *save_ptr, *token, *fn_copy;
  int count=0, i =0;

  //count the number of token, it uses copied data
  fn_copy = palloc_get_page(0);
  strlcpy(fn_copy,file_name,PGSIZE);
  for(token = strtok_r(file_name," ",&save_ptr);
	token !=NULL;token = strtok_r(NULL," ",&save_ptr))
    	count++;

  //allocate memory for saving tokens
  parse = (char**)malloc(sizeof(char*)*count);

  //parsing argument
  for(token = strtok_r(fn_copy," ",&save_ptr);
	token != NULL; token = strtok_r(NULL," ",&save_ptr))
  {
  	 parse[i] = token;
	 i++;
  }

  /* Initialize vm hash table */
  vm_init(&thread_current()->vm);

  /* Initialize interrupt frame and load executable. */
  memset (&if_, 0, sizeof if_);
  if_.gs = if_.fs = if_.es = if_.ds = if_.ss = SEL_UDSEG;
  if_.cs = SEL_UCSEG;
  if_.eflags = FLAG_IF | FLAG_MBS;

  success = load (file_name, &if_.eip, &if_.esp);

  /* If load failed, quit. */
  thread_current()->program_loaded = success;
  sema_up(&thread_current()->load_sema);

  if(success) argument_stack(parse,count,&if_.esp);

  palloc_free_page (file_name);
  palloc_free_page (fn_copy);
  if (!success)
  { 
	thread_current()->exit_status = -1;
	thread_exit ();
  }
 
  /* Start the user process by simulating a return from an
     interrupt, implemented by intr_exit (in
     threads/intr-stubs.S).  Because intr_exit takes all of its
     arguments on the stack in the form of a `struct intr_frame',
     we just point the stack pointer (%esp) to our stack frame
     and jump to it. */
  asm volatile ("movl %0, %%esp; jmp intr_exit" : : "g" (&if_) : "memory");
  NOT_REACHED ();

}

/* Waits for thread TID to die and returns its exit status.  If
   it was terminated by the kernel (i.e. killed due to an
   exception), returns -1.  If TID is invalid or if it was not a
   child of the calling process, or if process_wait() has already
   been successfully called for the given TID, returns -1
   immediately, without waiting.

   This function will be implemented in problem 2-2.  For now, it
   does nothing. */
int
process_wait (tid_t child_tid UNUSED) 
{
  //get child process descriptor
  struct thread * child = get_child_process(child_tid);
  int child_exit_status;
  if(child==NULL)
 	 return -1;

  //wait until child process terminate : use semaphore
  sema_down(&child->exit_sema);
  
  //save exit_status
  child_exit_status = child->exit_status;

  //remove child process descriptor
  remove_child_process(child);
  return child_exit_status;
}

/* Free the current process's resources. */
void
process_exit (void)
{
  struct thread *cur = thread_current ();
  uint32_t *pd;

  //close all open file
  int index = cur->fd_index;
  while(index > 2)
  {
	process_close_file(index-1);
	index--;
  }

  //free the memory of file descriptor table
  if(cur->run_file)
	file_close(cur->run_file);

  palloc_free_page(cur->fd_table); 

  /* free mmap_file */
  munmap(CLOSE_ALL);

  /* for test including 'merge', should free pages of dead thread */
  free_page_all(cur->tid);

  /* remove vm_entries */
  vm_destroy(&cur->vm);

  /* Destroy the current process's page directory and switch back
     to the kernel-only page directory. */
  pd = cur->pagedir;
  if (pd != NULL) 
    {
      /* Correct ordering here is crucial.  We must set
         cur->pagedir to NULL before switching page directories,
         so that a timer interrupt can't switch back to the
         process page directory.  We must activate the base page
         directory before destroying the process's page
         directory, or our active page directory will be one
         that's been freed (and cleared). */
      cur->pagedir = NULL;
      pagedir_activate (NULL);
      pagedir_destroy (pd);
    }
}

/* Sets up the CPU for running user code in the current
   thread.
   This function is called on every context switch. */
void
process_activate (void)
{
  struct thread *t = thread_current ();

  /* Activate thread's page tables. */
  pagedir_activate (t->pagedir);

  /* Set thread's kernel stack for use in processing
     interrupts. */
  tss_update ();
}

/* We load ELF binaries.  The following definitions are taken
   from the ELF specification, [ELF1], more-or-less verbatim.  */

/* ELF types.  See [ELF1] 1-2. */
typedef uint32_t Elf32_Word, Elf32_Addr, Elf32_Off;
typedef uint16_t Elf32_Half;

/* For use with ELF types in printf(). */
#define PE32Wx PRIx32   /* Print Elf32_Word in hexadecimal. */
#define PE32Ax PRIx32   /* Print Elf32_Addr in hexadecimal. */
#define PE32Ox PRIx32   /* Print Elf32_Off in hexadecimal. */
#define PE32Hx PRIx16   /* Print Elf32_Half in hexadecimal. */

/* Executable header.  See [ELF1] 1-4 to 1-8.
   This appears at the very beginning of an ELF binary. */
struct Elf32_Ehdr
  {
    unsigned char e_ident[16];
    Elf32_Half    e_type;
    Elf32_Half    e_machine;
    Elf32_Word    e_version;
    Elf32_Addr    e_entry;
    Elf32_Off     e_phoff;
    Elf32_Off     e_shoff;
    Elf32_Word    e_flags;
    Elf32_Half    e_ehsize;
    Elf32_Half    e_phentsize;
    Elf32_Half    e_phnum;
    Elf32_Half    e_shentsize;
    Elf32_Half    e_shnum;
    Elf32_Half    e_shstrndx;
  };

/* Program header.  See [ELF1] 2-2 to 2-4.
   There are e_phnum of these, starting at file offset e_phoff
   (see [ELF1] 1-6). */
struct Elf32_Phdr
  {
    Elf32_Word p_type;
    Elf32_Off  p_offset;
    Elf32_Addr p_vaddr;
    Elf32_Addr p_paddr;
    Elf32_Word p_filesz;
    Elf32_Word p_memsz;
    Elf32_Word p_flags;
    Elf32_Word p_align;
  };

/* Values for p_type.  See [ELF1] 2-3. */
#define PT_NULL    0            /* Ignore. */
#define PT_LOAD    1            /* Loadable segment. */
#define PT_DYNAMIC 2            /* Dynamic linking info. */
#define PT_INTERP  3            /* Name of dynamic loader. */
#define PT_NOTE    4            /* Auxiliary info. */
#define PT_SHLIB   5            /* Reserved. */
#define PT_PHDR    6            /* Program header table. */
#define PT_STACK   0x6474e551   /* Stack segment. */

/* Flags for p_flags.  See [ELF3] 2-3 and 2-4. */
#define PF_X 1          /* Executable. */
#define PF_W 2          /* Writable. */
#define PF_R 4          /* Readable. */

static bool setup_stack (void **esp);
static bool validate_segment (const struct Elf32_Phdr *, struct file *);
static bool load_segment (struct file *file, off_t ofs, uint8_t *upage,
                          uint32_t read_bytes, uint32_t zero_bytes,
                          bool writable);

/* Loads an ELF executable from FILE_NAME into the current thread.
   Stores the executable's entry point into *EIP
   and its initial stack pointer into *ESP.
   Returns true if successful, false otherwise. */
bool
load (const char *file_name, void (**eip) (void), void **esp) 
{
  struct thread *t = thread_current ();
  struct Elf32_Ehdr ehdr;
  struct file *file = NULL;
  off_t file_ofs;
  bool success = false;
  int i;

  /* Allocate and activate page directory. */
  t->pagedir = pagedir_create ();
  if (t->pagedir == NULL) 
    goto done;
  process_activate ();

  //before file open, lock the file system for preventing change data
  lock_acquire(&filesys_lock);

  /* Open executable file. */
  file = filesys_open (file_name);

  if (file == NULL) 
    {
      lock_release(&filesys_lock);
      printf ("load: %s: open failed\n", file_name);
      goto done; 
    }

  t->run_file = file;
  file_deny_write(file);

  //release lock
  lock_release(&filesys_lock);

  /* Read and verify executable header. */
  if (file_read (file, &ehdr, sizeof ehdr) != sizeof ehdr
      || memcmp (ehdr.e_ident, "\177ELF\1\1\1", 7)
      || ehdr.e_type != 2
      || ehdr.e_machine != 3
      || ehdr.e_version != 1
      || ehdr.e_phentsize != sizeof (struct Elf32_Phdr)
      || ehdr.e_phnum > 1024) 
    {
      printf ("load: %s: error loading executable\n", file_name);
      goto done; 
    }

  /* Read program headers. */
  file_ofs = ehdr.e_phoff;
  for (i = 0; i < ehdr.e_phnum; i++) 
    {
      struct Elf32_Phdr phdr;

      if (file_ofs < 0 || file_ofs > file_length (file))
        goto done;
      file_seek (file, file_ofs);

      if (file_read (file, &phdr, sizeof phdr) != sizeof phdr)
        goto done;
      file_ofs += sizeof phdr;
      switch (phdr.p_type) 
        {
        case PT_NULL:
        case PT_NOTE:
        case PT_PHDR:
        case PT_STACK:
        default:
          /* Ignore this segment. */
          break;
        case PT_DYNAMIC:
        case PT_INTERP:
        case PT_SHLIB:
          goto done;
        case PT_LOAD:
          if (validate_segment (&phdr, file)) 
            {
              bool writable = (phdr.p_flags & PF_W) != 0;
              uint32_t file_page = phdr.p_offset & ~PGMASK;
              uint32_t mem_page = phdr.p_vaddr & ~PGMASK;
              uint32_t page_offset = phdr.p_vaddr & PGMASK;
              uint32_t read_bytes, zero_bytes;
              if (phdr.p_filesz > 0)
                {
                  /* Normal segment.
                     Read initial part from disk and zero the rest. */
                  read_bytes = page_offset + phdr.p_filesz;
                  zero_bytes = (ROUND_UP (page_offset + phdr.p_memsz, PGSIZE)
                                - read_bytes);
                }
              else 
                {
                  /* Entirely zero.
                     Don't read anything from disk. */
                  read_bytes = 0;
                  zero_bytes = ROUND_UP (page_offset + phdr.p_memsz, PGSIZE);
                }
              if (!load_segment (file, file_page, (void *) mem_page,
                                 read_bytes, zero_bytes, writable))
                goto done;
            }
          else
            goto done;
          break;
        }
    }

  /* Set up stack. */
  if (!setup_stack (esp))
    goto done;

  /* Start address. */
  *eip = (void (*) (void)) ehdr.e_entry;

  success = true;

 done:
  /* We arrive here whether the load is successful or not. */
  //file_close (file);
  return success;
}

/* load() helpers. */

static bool install_page (void *upage, void *kpage, bool writable);

/* Checks whether PHDR describes a valid, loadable segment in
   FILE and returns true if so, false otherwise. */
static bool
validate_segment (const struct Elf32_Phdr *phdr, struct file *file) 
{
  /* p_offset and p_vaddr must have the same page offset. */
  if ((phdr->p_offset & PGMASK) != (phdr->p_vaddr & PGMASK)) 
    return false; 

  /* p_offset must point within FILE. */
  if (phdr->p_offset > (Elf32_Off) file_length (file)) 
    return false;

  /* p_memsz must be at least as big as p_filesz. */
  if (phdr->p_memsz < phdr->p_filesz) 
    return false; 

  /* The segment must not be empty. */
  if (phdr->p_memsz == 0)
    return false;
  
  /* The virtual memory region must both start and end within the
     user address space range. */
  if (!is_user_vaddr ((void *) phdr->p_vaddr))
    return false;
  if (!is_user_vaddr ((void *) (phdr->p_vaddr + phdr->p_memsz)))
    return false;

  /* The region cannot "wrap around" across the kernel virtual
     address space. */
  if (phdr->p_vaddr + phdr->p_memsz < phdr->p_vaddr)
    return false;

  /* Disallow mapping page 0.
     Not only is it a bad idea to map page 0, but if we allowed
     it then user code that passed a null pointer to system calls
     could quite likely panic the kernel by way of null pointer
     assertions in memcpy(), etc. */
  if (phdr->p_vaddr < PGSIZE)
    return false;

  /* It's okay. */
  return true;
}

/* Loads a segment starting at offset OFS in FILE at address
   UPAGE.  In total, READ_BYTES + ZERO_BYTES bytes of virtual
   memory are initialized, as follows:

        - READ_BYTES bytes at UPAGE must be read from FILE
          starting at offset OFS.

        - ZERO_BYTES bytes at UPAGE + READ_BYTES must be zeroed.

   The pages initialized by this function must be writable by the
   user process if WRITABLE is true, read-only otherwise.

   Return true if successful, false if a memory allocation error
   or disk read error occurs. */
static bool
load_segment (struct file *file, off_t ofs, uint8_t *upage,
              uint32_t read_bytes, uint32_t zero_bytes, bool writable) 
{
  ASSERT ((read_bytes + zero_bytes) % PGSIZE == 0);
  ASSERT (pg_ofs (upage) == 0);
  ASSERT (ofs % PGSIZE == 0);

  file_seek (file, ofs);
  while (read_bytes > 0 || zero_bytes > 0) 
    {
      size_t page_read_bytes = read_bytes < PGSIZE ? read_bytes : PGSIZE;
      size_t page_zero_bytes = PGSIZE - page_read_bytes;

      struct vm_entry * vme;

      vme = (struct vm_entry *)malloc(sizeof(struct vm_entry));

      /* set vm_entry members */
      vme->type = VM_BIN;
      vme->writable = writable;
      vme->is_loaded = false;
      vme->vaddr = upage;
      vme->file = file;
      vme->offset = ofs;
      vme->read_bytes = page_read_bytes;
      vme->zero_bytes = page_zero_bytes;

      insert_vme(&thread_current()->vm, vme);

      /*Get a page of memory.
      uint8_t *kpage = palloc_get_page (PAL_USER);
      if (kpage == NULL)
        return false;

      Load this page.
      if (file_read (file, kpage, page_read_bytes) != (int) page_read_bytes)
        {
          palloc_free_page (kpage);
          return false; 
        }
      memset (kpage + page_read_bytes, 0, page_zero_bytes);

      Add the page to the process's address space.
      if (!install_page (upage, kpage, writable)) 
        {
          palloc_free_page (kpage);
          return false; 
        }*/

      /* Advance. */
      read_bytes -= page_read_bytes;
      zero_bytes -= page_zero_bytes;
      ofs += page_read_bytes;
      upage += PGSIZE;
    }
  return true;
}

/* Create a minimal stack by mapping a zeroed page at the top of
   user virtual memory. */
static bool
setup_stack (void **esp) 
{
  bool success = false;

  struct vm_entry * vme = (struct vm_entry *)malloc(sizeof(struct vm_entry));

  vme = (struct vm_entry *)malloc(sizeof(struct vm_entry));
  if(vme == NULL)
	return false;
  
  /* set vm_entry members */
  vme->vaddr = ((uint8_t*)PHYS_BASE) - PGSIZE;
  vme->type = VM_ANON;
  vme->writable = true;
  vme->is_loaded = true;

  struct page * page = alloc_page(PAL_USER| PAL_ZERO);
  page->vme = vme;
  void * kaddr = page->kaddr; 

  insert_vme(&thread_current()->vm, vme);

  if(!install_page(vme->vaddr, kaddr, vme->writable))
  {
	free_page(page);
	return success;
  }
 
  success = true;
  
  if(success)
	*esp = PHYS_BASE;

    return success;
}

/* Adds a mapping from user virtual address UPAGE to kernel
   virtual address KPAGE to the page table.
   If WRITABLE is true, the user process may modify the page;
   otherwise, it is read-only.
   UPAGE must not already be mapped.
   KPAGE should probably be a page obtained from the user pool
   with palloc_get_page().
   Returns true on success, false if UPAGE is already mapped or
   if memory allocation fails. */
static bool
install_page (void *upage, void *kpage, bool writable)
{
  struct thread *t = thread_current ();

  /* Verify that there's not already a page at that virtual
     address, then map our page there. */
  return (pagedir_get_page (t->pagedir, upage) == NULL
          && pagedir_set_page (t->pagedir, upage, kpage, writable));
}

bool 
handle_mm_fault(struct vm_entry * vme)
{

	bool ret = false;

	struct page * page;

	/* allocation memory */
	page = alloc_page(PAL_USER);
	page->vme = vme;
	void * kaddr = page->kaddr;

	if(vme->is_loaded)
		return ret;
	
	/* if type is VM_BIN or VM_FILE, use load_file
	   if type is VM_ANON, use swap_in */
	switch(vme->type)
	{
		case VM_BIN:
			ret = load_file(kaddr, vme);
		case VM_FILE:
			ret = load_file(kaddr, vme);
			break;
		case VM_ANON:
			swap_in(vme->swap_slot, page->kaddr);
			ret = true;
			break;
		default:
			break;
	}
	
	if(!ret)
	{
		free_page(page);
		return false;
	}

	/* mapping physical page and virtual page */
	install_page(vme->vaddr, kaddr, vme->writable);

	vme->is_loaded = true;

	return ret;
}
